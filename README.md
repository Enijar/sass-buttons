# SASS Buttons Library #

Simple SASS buttons library that is easy to extend button style. You can see some [examples here](http://labs.enijar.net/buttons/).

### How do I get set up? ###

Simple include the buttons.scss file into your main.scs file: ```@include('buttons')```

### How do I customise buttons? ###

Add this to ```buttons.scss``` or any file loaded in after ```buttons.scss```

```
#!scss
$turquoise: #43ccb5;

.btn.turquoise { @include button($turquoise) }

.btn.turquoise {
  padding: 20px 35px;
  font-size: 18px;
  border-radius: 25px;
}

```

The button mixin is also customisable directly. The parameters are:

```
#!scss
.btn.turquoise {
  @include button($color, $radius, $width, $height, $box-shadow);
}

```

Customised example:

```
#!scss
.btn.turquoise {
  @include button($turquoise, 15px, auto, auto, 0 2px 1px mix(#fff, $turquoise, 50%));
}

```